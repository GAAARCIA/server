# 3.3. Practica | Crear un Blockchain con node.js

	Repositorio de la practica 3.3 de la materia:
		Desarrollo Basado en Plataformas

Vamos a aplicar nuestros conocimientos adquiridos de Javascript y de Node.Js 
para programar un blockchain, para esto sigue los siguientes pasos:

	1) Genera una clase Block como modelo con los siguientes atributos: 

		a) Index = Identificador de la posición del bloque en la cadena.
		b) Data = El contenido del bloque
		c) previousHash = Valor del bloque anterior de la cadena

	2) Genera una clase Blockchain.

	3) Mediante la clase Blockchain manipula elementos Block para poder generar la cadena de bloques.

	4) Genera un método mine que calcule el nuevo hash del bloque según su dificultad.


# Prerequisitos

Para ejecutar los programas debe de contar con alguna versión de Node.js

A continuación se describen los pasos para descargar e instalar Node.js

## Instalando

Para descargar e instalar Node.js se utiliza el comando:

	apt-get install nodejs

Para actualizar Node.js, se utiliza el comando:

	apt-get update

# Corriendo pruebas

Para ejecutar el MAIN se usa el comando:

	node main.js

# Construido con

	Atom - Editor de texto
	Node.js - Entorno para ejecutar JavaScript

# Contribuciones

	Luis Antonio Ramírez Martínez, profesor de la Universidad Autónoma de Chihuahua
	que imparte la materia de Desarrollo Basado en Plataformas

# Autores

	Norma Angélica García Martínez
		329528
